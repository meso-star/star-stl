/* Copyright (C) 2015, 2016, 2019, 2021, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SSTL_H
#define SSTL_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(SSTL_SHARED_BUILD)
  #define SSTL_API extern EXPORT_SYM /* Build shared library */
#elif defined(SSTL_STATUC) /* Use/build statuc library */
  #define SSTL_API extern LOCAL_SYM
#else /* Use shared library */
  #define SSTL_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the sstl function `Func'
 * returns an error. One should use this macro on sstl function calls for which
 * no explicit error checking is performed. */
#ifndef NDEBUG
  #define SSTL(Func) ASSERT(sstl_ ## Func == RES_OK)
#else
  #define SSTL(Func) sstl_ ## Func
#endif

/* The type of a read file */
enum sstl_read_type {
  SSTL_ASCII = 1,
  SSTL_BINARY = 2
};

/* Descriptor of a loaded STL */
struct sstl_desc {
  const char* solid_name; /* May be NULL <=> no name */
  enum sstl_read_type read_type; /* The type of the file */

  /* Front faces are CCW ordered and the normals follow the right handed rule */
  float* vertices; /* triangles_count * 3 coordinates */
  unsigned* indices; /* triangles_count * 3 indices */
  float* normals; /* Per triangle normalized normal */

  size_t triangles_count;
  size_t vertices_count;
};

/* Data descriptor used to write a bunch of triangles to a file in the STL
 * format.
 * - name can be NULL,
 * - print_error_log can be NULL: no log,
 * - data can be NULL if get_triangle_xxx functions do not use it. */
struct sstl_write_data {
  const char* name;
  const void* data;
  res_T (*get_triangle_vertices)
    (const unsigned idx, const void* data, float vtx[3][3]);
  res_T (*get_triangle_normal)
    (const unsigned idx, const void* data, float normal[3]);
  void (*print_error_log)(const void* data, const char* msg, ...);
  unsigned triangles_count;
};
#define SSTL_WRITE_DATA_NULL__ { NULL, NULL, NULL, NULL, NULL, 0 }
static const struct sstl_write_data SSTL_WRITE_DATA_NULL = SSTL_WRITE_DATA_NULL__;

/* Forward declaration of external types */
struct logger;
struct mem_allocator;

/* Forward declaration of opaque data types */
struct sstl;

/*******************************************************************************
 * Star-STL API
 ******************************************************************************/
BEGIN_DECLS

SSTL_API res_T
sstl_create
  (struct logger* logger, /* NULL <=> use default logger*/
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   const int verbose, /* Verbosity level */
   struct sstl** sstl);

SSTL_API res_T
sstl_ref_get
  (struct sstl* sstl);

SSTL_API res_T
sstl_ref_put
  (struct sstl* sstl);

SSTL_API res_T
sstl_load_ascii
  (struct sstl* sstl,
   const char* filename);

SSTL_API res_T
sstl_load_stream_ascii
  (struct sstl* sstl,
   FILE* stream);

SSTL_API res_T
sstl_load_binary
  (struct sstl* sstl,
   const char* filename);

SSTL_API res_T
sstl_load_stream_binary
  (struct sstl* sstl,
   FILE* stream);

/* Try first loading as an ASCII file, then as a binary file if ASCII failed.
 * Note that a binary file starting with "solid" will be wrongly identified as
 * ASCII, thus leading to a failure without trying a binary load.
 * Also warning and error messages will possibly report on both attempts. */
SSTL_API res_T
sstl_load
  (struct sstl* sstl,
   const char* filename);

/* Try first loading as an ASCII stream, then as a binary stream if ASCII failed.
 * Note that a binary stream starting with "solid" will be wrongly identified as
 * ASCII, thus leading to a failure without trying a binary load.
 * Also warning and error messages will possibly report on both attempts. */
SSTL_API res_T
sstl_load_stream
  (struct sstl* sstl,
   FILE* stream);

/* Create a sstl_write_data from a sstl.
 * The result is valid as long as the input sstl is valid */
SSTL_API res_T
sstl_pack_write_data
  (const struct sstl* sstl,
   struct sstl_write_data* out);

SSTL_API res_T
sstl_write
  (const struct sstl_write_data* data,
   const int binary,
   const char* filename);

SSTL_API res_T
sstl_write_stream
  (const struct sstl_write_data* data,
   const int binary,
   FILE* stream);

/* The returned descriptor is valid until a new load process */
SSTL_API res_T
sstl_get_desc
  (struct sstl* sstl,
   struct sstl_desc* desc);

END_DECLS

#endif /* SSTL_H */

