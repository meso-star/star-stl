/* Copyright (C) 2015, 2016, 2019, 2021, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* strtok_r support */

#include "sstl.h"

#include <rsys/rsys.h>
#include <rsys/cstr.h>
#include <rsys/float3.h>
#include <rsys/hash_table.h>
#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>
#include <rsys/stretchy_array.h>

#include <string.h>
#include <stdio.h>
#include <ctype.h>

#ifdef COMPILER_CL
  #pragma warning(push)
  #pragma warning(disable:4706) /* Assignment within a condition */

  #define strtok_r strtok_s
#endif

#define OK(Expr) if((res = (Expr)) != RES_OK) goto error; else (void)0

enum allowed_load_steps {
  TRY_READ_ASCII = 1,
  TRY_READ_BINARY = 2,
  TRY_READ_ALL = 3
};

enum read_type {
  STARTED_ASCII = BIT(0),
  STARTED_BIN = BIT(1),
  OK_ASCII = BIT(2),
  OK_BINARY = BIT(3)
};

struct solid {
  char* name;
  unsigned* indices;
  float* vertices;
  float* normals;
  enum read_type read_type;
};
static const struct solid SOLID_NULL = { NULL, NULL, NULL, NULL, 0 };

struct streamer {
  FILE* stream;
  const char* name;
  size_t iline;
  char* buf;
};

struct vertex { float xyz[3]; };

static INLINE char
eq_vertex(const struct vertex* a, const struct vertex* b)
{
  return (char)f3_eq(a->xyz, b->xyz);
}

/* Declare the hash table that map a vertex to its index */
#define HTABLE_NAME vertex
#define HTABLE_DATA unsigned
#define HTABLE_KEY struct vertex
#define HTABLE_KEY_FUNCTOR_EQ eq_vertex
#include <rsys/hash_table.h>

struct sstl {
  int verbose;
  struct htable_vertex vertex2id;
  struct solid solid;

  struct logger* logger;
  struct mem_allocator* allocator;
  ref_T ref;
};

/* A type for binary files I/O */
struct tmp {
  float normal[3];
  union u {
    float vertices[3][3];
    struct vertex vtx[3];
  } u;
  unsigned short attrib, foo;
};

STATIC_ASSERT(sizeof(struct tmp) == 52, Invalid_struct_size);

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
streamer_init(struct streamer* streamer, FILE* stream, const char* name)
{
  ASSERT(streamer && stream && name);
  memset(streamer, 0, sizeof(struct streamer));
  streamer->stream = stream;
  streamer->name = name;
  streamer->iline = 0;
  streamer->buf = sa_add(streamer->buf, 128);
  ASSERT(streamer->buf);
}

static void
streamer_release(struct streamer* streamer)
{
  ASSERT(streamer);
  sa_release(streamer->buf);
}

/* Large enough to read "solid\0" */
#define CHECK_SOLID_LEN 6

static char*
streamer_read_line
  (struct streamer* streamer,
   int checking_solid,
   size_t* count)
{
  const size_t buf_chunk = 256;
  size_t read_sz, read_count = 0;
  char* line;
  ASSERT(streamer && count);

  if(checking_solid) {
    size_t current = sa_size(streamer->buf);
    if(current < CHECK_SOLID_LEN) {
      char* remain = sa_add(streamer->buf, CHECK_SOLID_LEN - current);
      if(!remain) {
        FATAL("Not enough memory: couldn't resize the stream buffer.\n");
      }
    }
    read_sz = CHECK_SOLID_LEN;
  } else {
    read_sz = sa_size(streamer->buf);
  }

  for(;(line=fgets(streamer->buf, (int)read_sz, streamer->stream));
      ++streamer->iline) {
    size_t l;

    /* If checking for a line starting with "solid", stop reading early if the
     * line does not. If no error, ensure that the whole line is read */
    while(!strrchr(line, '\n')) {
      char* remain;
      size_t remain_sz;
      if(checking_solid) {
        if(0 != strncmp("solid", line, 5)) {
          l = strlen(line);
          read_count += l;
          *count = read_count;
          /* Dont read further */
          return line;
        }
        remain = line + 5;
        ASSERT(*remain == '\0');
        remain_sz = sa_size(streamer->buf) - 5;
      } else {
        remain = sa_add(streamer->buf, buf_chunk);
        if(!remain) {
          FATAL("Not enough memory: couldn't resize the stream buffer.\n");
        }
        remain_sz = buf_chunk;
      }
      line = streamer->buf;
      if(!fgets(remain, (int)remain_sz, streamer->stream)) /* EOF */
        break;

      read_sz = sa_size(streamer->buf);
      checking_solid = 0;
    }

    l = strlen(streamer->buf);
    read_count += l;
    if(strspn(streamer->buf, " \t\r\n") != l) { /* Not empty */
      /* Remove newline character(s) */
      size_t last_char = strlen(line);
      while(last_char-- && (line[last_char]=='\n' || line[last_char]=='\r'));
      line[last_char + 1] = '\0';
      break;
    }
  }
  ++streamer->iline;
  *count += read_count;
  return line;
}

static void
solid_clear(struct solid* solid)
{
  ASSERT(solid);
  sa_release(solid->name);
  sa_release(solid->vertices);
  sa_release(solid->indices);
  sa_release(solid->normals);
}

static void
clear(struct sstl* sstl)
{
  ASSERT(sstl);
  solid_clear(&sstl->solid);
  sstl->solid = SOLID_NULL;
  htable_vertex_clear(&sstl->vertex2id);
}

static void
print_log
  (const struct sstl* sstl, const enum log_type type, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(sstl && msg);
  if(sstl->verbose) {
    res_T res; (void)res;
    va_start(vargs_list, msg);
    res = logger_vprint(sstl->logger, type, msg, vargs_list);
    ASSERT(res == RES_OK);
    va_end(vargs_list);
  }
}

static INLINE res_T
parse_float3
  (struct sstl* sstl,
   char* str,
   float vert[3],
   const char* filename,
   const size_t iline,
   char** tok_ctx)
{
  char* tk;
  int i;
  res_T res = RES_OK;
  ASSERT(str && vert && filename);

  FOR_EACH(i, 0, 3) {
    tk = strtok_r(i==0 ? str : NULL, " \t", tok_ctx);
    if(!tk) {
      print_log(sstl, LOG_ERROR, "%s:%lu: expecting 3D coordinates.\n",
        filename, (unsigned long)iline);
      return RES_BAD_ARG;
    }

    res = cstr_to_float(tk, vert + i);
    if(res != RES_OK) {
      print_log(sstl, LOG_ERROR, "%s:%lu: invalid coordinate \"%s\".\n",
        filename, (unsigned long)iline, tk);
      return res;
    }
  }
  tk = strtok_r(NULL, "\0", tok_ctx);
  if(tk) { /* Unexpected remaining chars */
    print_log(sstl, LOG_WARNING, "%s:%lu: unexpected directive \"%s\".\n",
      filename, (unsigned long)iline, tk);
  }
  return RES_OK;
}

static INLINE res_T
parse_name_string
  (struct sstl* sstl,
   char* str,
   char** out_name,
   const char* filename,
   const size_t iline,
   char** tok_ctx)
{
  char* name = NULL;
  char* tk;
  res_T res = RES_OK;
  ASSERT(sstl && out_name);

  if(!str) goto exit;

  /* Handle name with spaces */
  for(tk = strtok_r(str, " \t", tok_ctx); tk; tk = strtok_r(NULL, " \t", tok_ctx)) {
    char* remain = NULL;
    if(name) name[strlen(name)] = ' '; /* Replace '\0' by ' ' */
    remain = sa_add(name, strlen(tk) + 1/*NULL char*/);
    if(!remain) {
      print_log(sstl, LOG_ERROR,
        "%s:%lu: not enough memory: couldn't allocate the name of the solid.\n",
        filename, (unsigned long)iline);
      res = RES_MEM_ERR;
      goto error;
    }
    strcpy(remain, tk);
  }

exit:
  *out_name = name;
  return res;
error:
  if(name) {
    sa_release(name);
    name = NULL;
  }
  goto exit;
}

static INLINE res_T
parse_solid_name
  (struct sstl* sstl,
   struct solid* solid,
   char* line,
   const char* filename,
   const size_t iline,
   int allowed,
   char** tok_ctx)
{
  res_T res = RES_OK;
  ASSERT(sstl && solid && !solid->name);

  if(!line || strcmp(strtok_r(line, " \t", tok_ctx), "solid")) {
    enum log_type lt = (allowed & TRY_READ_BINARY) ? LOG_OUTPUT : LOG_ERROR;
    print_log(sstl, lt,
      "%s:%lu: missing the \"solid [NAME]\" directive.\n",
      filename, (unsigned long)iline);
    res = RES_BAD_ARG;
    goto error;
  }

  solid->read_type |= STARTED_ASCII;

  OK(parse_name_string(sstl, strtok_r(NULL, "\0", tok_ctx), &solid->name,
       filename, iline, tok_ctx));

exit:
  return res;
error:
  if(solid->name) sa_release(solid->name);
  goto exit;
}

static INLINE res_T
parse_solid_vertex
  (struct sstl* sstl,
   struct solid* solid,
   unsigned* const index,
   char* line,
   const char* filename,
   const size_t iline,
   char** tok_ctx)
{
  struct vertex vertex;
  unsigned* found_id;
  res_T res = RES_OK;
  ASSERT(sstl && solid && index);

  if(!line || strcmp(strtok_r(line, " \t", tok_ctx), "vertex")) {
    print_log(sstl, LOG_ERROR,
      "%s:%lu: missing a \"vertex X Y Z\" directive.\n",
      filename, (unsigned long)iline);
    return RES_BAD_ARG;
  }

  res = parse_float3(sstl, strtok_r(NULL, "\0", tok_ctx), vertex.xyz, filename, iline, tok_ctx);
  if(res != RES_OK) return res;

  /* Look for an already registered vertex position */
  found_id = htable_vertex_find(&sstl->vertex2id, &vertex);

  if(found_id) {
    *index = *found_id;
  } else {
    /* Add a new vertex */
    *index = (unsigned)sa_size(solid->vertices) / 3;
    res = htable_vertex_set(&sstl->vertex2id, &vertex, index);
    if(res != RES_OK) {
      print_log(sstl, LOG_ERROR,
        "%s:%lu: couldn't register a vertex position.\n",
        filename, (unsigned long)iline);
    }
    f3_set(sa_add(solid->vertices, 3), vertex.xyz);
  }
  sa_push(solid->indices, *index);

  return RES_OK;
}

static INLINE res_T
parse_outer_loop
  (struct sstl* sstl,
   char* line,
   const char* filename,
   const size_t iline,
   char** tok_ctx)
{
  char* tk;
  ASSERT(sstl);

  if(!line
  || strcmp(strtok_r(line, " \t", tok_ctx), "outer")
  || !(tk = strtok_r(NULL, " \t", tok_ctx))
  || strcmp(tk, "loop")) {
    print_log(sstl, LOG_ERROR,
      "%s:%lu: missing the \"outer loop\" directive.\n",
      filename, (unsigned long)iline);
    return RES_BAD_ARG;
  }

  tk = strtok_r(NULL, "\0", tok_ctx);
  if(tk && strspn(tk, " \t\r\n") != strlen(tk)) { /* Invalid remaining chars */
    print_log(sstl, LOG_WARNING,
      "%s:%lu: malformed \"outer loop\" directive.\n",
      filename, (unsigned long)iline);
  }
  return RES_OK;
}

static INLINE res_T
parse_directive
  (struct sstl* sstl,
   const char* directive,
   char* line,
   const char* filename,
   const size_t iline,
   char** tok_ctx)
{
  char* tk;
  ASSERT(sstl && directive);

  if(!line || strcmp(strtok_r(line, " \t", tok_ctx), directive)) {
    print_log(sstl, LOG_ERROR,
      "%s:%lu: missing the \"%s\" directive.\n",
      filename, (unsigned long)iline, directive);
    return RES_BAD_ARG;
  }

  tk = strtok_r(NULL, " \0", tok_ctx);
  if(tk && strspn(tk, " \t\r\n") != strlen(tk)) { /* Invalid remaining chars */
    print_log(sstl, LOG_WARNING,
      "%s:%lu: malformed \"%s\" directive.\n",
      filename, (unsigned long)iline, directive);
  }

  return RES_OK;
}

static res_T
load_ascii_stream
  (struct sstl* sstl,
   FILE* stream,
   const char* stream_name,
   int allowed,
   size_t* count)
{
  res_T res = RES_OK;
  struct streamer streamer;
  struct solid solid = SOLID_NULL;
  char* line = NULL;
  char* tok_ctx;
  char* tk;

  ASSERT(sstl && stream && stream_name && count);
  streamer_init(&streamer, stream, stream_name);
  clear(sstl);

  line = streamer_read_line(&streamer, 1, count);
  OK(parse_solid_name(sstl, &solid, line, streamer.name, streamer.iline,
        allowed, &tok_ctx));

  for(;;) { /* Parse the solid facets */
    float normal[3];
    unsigned facet[3];
    int ivertex;

    line = streamer_read_line(&streamer, 0, count);
    if(!line) {
      print_log(sstl, LOG_ERROR, "%s:%lu: missing directive.\n",
        streamer.name, (unsigned long)streamer.iline);
      res = RES_BAD_ARG;
      goto error;
    }

    tk = strtok_r(line, " \t", &tok_ctx);

    /* Stop on "endsolid" directive */
    if(!strcmp(tk, "endsolid"))
      break;

    /* Parse the facet normal directive */
    if(strcmp(tk, "facet")
    || !(tk = strtok_r(NULL, " \t", &tok_ctx))
    || strcmp(tk, "normal")) {
      print_log(sstl, LOG_ERROR,
        "%s:%lu: missing or malformed \"facet normal X Y Z\" directive.\n",
        streamer.name, (unsigned long)streamer.iline);
      res = RES_BAD_ARG;
      goto error;
    }
    OK(parse_float3(sstl, strtok_r(NULL, "\0", &tok_ctx), normal, streamer.name,
         streamer.iline, &tok_ctx));

    /* Parse the Outer loop directive */
    line = streamer_read_line(&streamer, 0, count);
    OK(parse_outer_loop(sstl, line, streamer.name, streamer.iline, &tok_ctx));

    /* Parse the facet vertices. Assume that only 3 vertices are submitted */
    FOR_EACH(ivertex, 0, 3) {
      line = streamer_read_line(&streamer, 0, count);
      OK(parse_solid_vertex(sstl, &solid, facet+ivertex, line, streamer.name,
           streamer.iline, &tok_ctx));
    }

    if(!f3_is_normalized(normal)) { /* Use geometry normal */
      float v0[3], v1[3];
      /* Vertices are CCW ordered and the normal follows the right handed rule */
      f3_sub(v0, solid.vertices + facet[1]*3, solid.vertices + facet[0]*3);
      f3_sub(v1, solid.vertices + facet[2]*3, solid.vertices + facet[0]*3);
      f3_cross(normal, v0, v1);
      f3_normalize(normal, normal);
    }
    f3_set(sa_add(solid.normals, 3), normal);

    line = streamer_read_line(&streamer, 0, count);
    OK(parse_directive(sstl, "endloop", line, streamer.name, streamer.iline, &tok_ctx));

    line = streamer_read_line(&streamer, 0, count);
    OK(parse_directive(sstl, "endfacet", line, streamer.name, streamer.iline, &tok_ctx));
  }

  /* Check the solid/endsolid name consistency */
  if(sstl->verbose && solid.name) {
    char* name = NULL;
    OK(parse_name_string(sstl, strtok_r(NULL, "\0", &tok_ctx), &name,
         streamer.name, streamer.iline, &tok_ctx));

    /* Compare the "endsolid" name with the one of the "solid" directive */
    if(name && strcmp(name, solid.name)) {
      print_log(sstl, LOG_WARNING,
        "%s:%lu: inconsistent \"endsolid\" name.\n",
        streamer.name, (unsigned long)streamer.iline);
    }
    sa_release(name);
  }
  if(sstl->verbose) {
    int tmp, non_space = 0;
    size_t i = 0;
    while (EOF != (tmp = fgetc(stream))) {
      i++;
      if(!isspace(tmp)) non_space = 1;
    }
    if(non_space) {
      print_log(sstl, LOG_WARNING,
          "%s: %u unexpected trailing characters.\n",
          stream_name, i);
    }
  }

  /* Register the solid */
  solid.read_type |= OK_ASCII;
  sstl->solid = solid;

exit:
  streamer_release(&streamer);
  return res;
error:
  solid_clear(&solid);
  goto exit;
}

static res_T
load_binary_stream
  (struct sstl* sstl,
   FILE* stream,
   const char* stream_name,
   const size_t already_read_count)
{
  res_T res = RES_OK;
  char header[80];
  unsigned triangles_count;
  struct solid solid = SOLID_NULL;
  unsigned i;

  ASSERT(sstl && stream && stream_name && already_read_count <= 80);
  clear(sstl);

  /* Read header; the first 80 bytes are meaningless and can be safely
   * (partially) dropped */
  if(1 != fread(header+already_read_count, sizeof(header)-already_read_count, 1,
       stream))
  {
    print_log(sstl, LOG_ERROR, "%s: missing header.\n", stream_name);
    res = RES_BAD_ARG;
    goto error;
  }

  solid.read_type |= STARTED_BIN;

  if(1 != fread(&triangles_count, 4, 1, stream)) {
    print_log(sstl, LOG_ERROR, "%s: missing triangle count.\n", stream_name);
    res = RES_BAD_ARG;
    goto error;
  }
  for(i = 0; i < triangles_count; i++) {
    struct tmp tmp;
    int n;
    if(1 != fread(&tmp, 50, 1, stream)) {
      print_log(sstl, LOG_ERROR, "%s: missing triangle data.\n", stream_name);
      res = RES_BAD_ARG;
      goto error;
    }

    f3_set(sa_add(solid.normals, 3), tmp.normal);
    for(n = 0; n < 3; n++) {
      /* Look for an already registered vertex position */
      unsigned* found_id = htable_vertex_find(&sstl->vertex2id, &tmp.u.vtx[n]);
      unsigned index;

      if(found_id) {
        index = *found_id;
      } else {
        /* Add a new vertex */
        index = (unsigned)sa_size(solid.vertices) / 3;
        res = htable_vertex_set(&sstl->vertex2id, &tmp.u.vtx[n], &index);
        if(res != RES_OK) {
          print_log(sstl, LOG_ERROR,
            "%s:%u: couldn't register a vertex position.\n",
            stream_name, i);
        }
        f3_set(sa_add(solid.vertices, 3), tmp.u.vtx[n].xyz);
      }
      sa_push(solid.indices, index);
    }
  }
  if(sstl->verbose) {
    i = 0;
    while (1 == fread(header, 1, 1, stream)) i++;
    if(i) {
      print_log(sstl, LOG_WARNING,
          "%s: %u unexpected trailing bytes.\n",
          stream_name, i);
    }
  }

  /* Register the solid */
  solid.read_type |= OK_BINARY;
  sstl->solid = solid;

exit:
  return res;
error:
  solid_clear(&solid);
  goto exit;
}

static res_T
load_stream
  (struct sstl* sstl,
   FILE* stream,
   const char* stream_name,
   int allowed)
{
  res_T res = RES_OK;
  int log = (allowed == TRY_READ_ALL);
  size_t count = 0;

  ASSERT(sstl && stream && allowed);

  /* Try reading as an ASCII file; if the file doesn't start with "solid" the
   * file is not read past the first 5 characters, so that we can continue
   * reading as a binary file, exploiting the fact that binary files' first 80
   * characters are meaningless */
  if(allowed & TRY_READ_ASCII) {
    if(log) {
      print_log(sstl, LOG_OUTPUT,
        "%s: attempt to read as ASCII file.\n", stream_name);
    }
    res = load_ascii_stream(sstl, stream, stream_name, allowed, &count);
    if(res == RES_OK) {
      if(log) print_log(sstl, LOG_OUTPUT, "Attempt successful.\n");
      goto exit;
    }
  }
  /* If here the stream could not be read as ASCII */
  if(!(allowed & TRY_READ_BINARY)) goto exit;
  if(sstl->solid.read_type & STARTED_ASCII) {
    /* "solid" was found: was an ill-formed ASCII file */
    return res;
  }
  if(count > 80) {
    /* Don't know if can happen, but further code cannot handle this */
    print_log(sstl, LOG_ERROR,
      "%s: cannot attempt to read as binary file"
      " (too many bytes read while trying ascii read).\n",
      stream_name);
    res = RES_BAD_ARG;
    goto error;
  }
  if(log) {
    print_log(sstl, LOG_OUTPUT,
      "%s: attempt to read as binary file.\n", stream_name);
  }
  res = load_binary_stream(sstl, stream, stream_name, count);
  if(res != RES_OK) goto error;
  if(log) print_log(sstl, LOG_OUTPUT, "Attempt successful.\n");

exit:
  return res;
error:
  goto exit;
}

#undef FTELL

static res_T
load_base(struct sstl* sstl, const char* filename, int allowed)
{
  FILE* file;
  res_T res = RES_OK;

  file = fopen(filename, "r");
  if(!file) {
    print_log(sstl, LOG_ERROR, "Error opening `%s'.\n", filename);
    return RES_IO_ERR;
  }
  res = load_stream(sstl, file, filename, allowed);
  fclose(file);
  return res;
}

static res_T
get_sstl_triangle_normal
  (const unsigned idx,
   const void* data,
   float normal[3])
{
  res_T res = RES_OK;
  struct sstl* sstl = (struct sstl*)data;
  struct sstl_desc desc;

  if(!sstl || !normal) {
    res = RES_BAD_ARG;
    goto error;
  }

  OK(sstl_get_desc(sstl, &desc));
  if(idx >= desc.triangles_count) {
    res = RES_BAD_ARG;
    goto error;
  }

  ASSERT(3*idx+2 < sa_size(desc.normals));
  f3_set(normal, desc.normals + 3*idx);

exit:
  return res;
error:
  goto exit;
}

static res_T
get_sstl_triangle_vertices
  (const unsigned idx,
   const void* data,
   float vtx[3][3])
{
  res_T res = RES_OK;
  struct sstl* sstl = (struct sstl*)data;
  struct sstl_desc desc;
  unsigned n;

  if(!sstl || !vtx) {
    res = RES_BAD_ARG;
    goto error;
  }

  OK(sstl_get_desc(sstl, &desc));
  if(idx >= desc.triangles_count) {
    res = RES_BAD_ARG;
    goto error;
  }

  for(n = 0; n < 3; n++) {
    size_t vtx_idx = desc.indices[3*idx + n];
    ASSERT(3*vtx_idx+2 < sa_size(desc.vertices));
    f3_set(vtx[n], desc.vertices + 3*vtx_idx);
  }

exit:
  return res;
error:
  goto exit;
}

static void
print_sstl_error_log
  (const void* data,
   const char* msg,
   ...)
{
  const struct sstl_write_data* sstld = (struct sstl_write_data*)data;
  va_list vargs_list;
  va_start(vargs_list, msg);
  print_log(sstld->data, LOG_ERROR, msg, vargs_list);
  va_end(vargs_list);
}

res_T
sstl_pack_write_data
  (const struct sstl* sstl,
   struct sstl_write_data* out)
{
  size_t sz;
  if(!sstl || !out) return RES_BAD_ARG;
  if(!sstl->solid.indices || !sstl->solid.normals || !sstl->solid.vertices)
    return RES_BAD_ARG;
  sz = sa_size(sstl->solid.indices);
  if(sz % 3 != 0) return RES_BAD_ARG;
  sz /= 3;
  if(sz > UINT_MAX) return RES_BAD_ARG;
  out->name = sstl->solid.name;
  out->data = sstl;
  out->get_triangle_normal = get_sstl_triangle_normal;
  out->get_triangle_vertices = get_sstl_triangle_vertices;
  out->print_error_log = print_sstl_error_log;
  out->triangles_count = (unsigned)sz;
  return RES_OK;
}

#define OKP(Expr) if((Expr) < 0) { res=RES_IO_ERR; goto error; }

static res_T
write_stream
  (const struct sstl_write_data* data,
   FILE* stream,
   const char* stream_name,
   const int binary)
{
  res_T res = RES_OK;
  unsigned i;

  ASSERT(data && stream && stream_name);

  if(data->get_triangle_normal == NULL) {
    if(data->print_error_log) {
      data->print_error_log(data,
         "%s: no getter defined for triangles' normals.\n", stream_name);
    }
    res = RES_BAD_ARG;
    goto error;
  }

  if(data->get_triangle_vertices == NULL) {
    if(data->print_error_log) {
      data->print_error_log(data,
         "%s: no getter defined for triangles' vertices.\n", stream_name);
    }
    res = RES_BAD_ARG;
    goto error;
  }

  if(binary) {
    char header[80] = "Binary STL written by the star-stl library";
    if(1 != fwrite(header, sizeof(header), 1, stream)) {
      res = RES_IO_ERR;
      goto error;
    }
    ASSERT(sizeof(data->triangles_count == 4));
    if(1 != fwrite(&data->triangles_count, 4, 1, stream)) {
      res = RES_IO_ERR;
      goto error;
    }
    for(i = 0; i < data->triangles_count; i++) {
      struct tmp tmp;
      res = data->get_triangle_normal(i, data->data, tmp.normal);
      res = data->get_triangle_vertices(i, data->data, tmp.u.vertices);
      tmp.attrib = 0;
      if(1 != fwrite(&tmp, 50, 1, stream)) {
        res = RES_IO_ERR;
        goto error;
      }
    }
  } else {
    if(data->name) {
      OKP(fprintf(stream, "solid %s\n", data->name));
    } else {
      OKP(fprintf(stream, "solid \n"));
    }
    for(i = 0; i < data->triangles_count; i++) {
      float normal[3], vtx[3][3];
      int n;

      res = data->get_triangle_normal(i, data->data, normal);
      OKP(fprintf(stream, "  facet normal %g %g %g\n", SPLIT3(normal)));

      res = data->get_triangle_vertices(i, data->data, vtx);
      OKP(fprintf(stream, "    outer loop\n"));
      for(n = 0; n < 3; n++) {
        OKP(fprintf(stream, "      vertex %.16g %.16g %.16g\n", SPLIT3(vtx[n])));
      }
      OKP(fprintf(stream, "    endloop\n"));
      OKP(fprintf(stream, "  endfacet\n"));
    }
    OKP(fprintf(stream, "endsolid \n"));
  }

exit:
  return res;
error:
  goto exit;
}

#undef OKP

static void
sstl_release(ref_T* ref)
{
  struct sstl* sstl;
  ASSERT(ref);
  sstl = CONTAINER_OF(ref, struct sstl, ref);
  clear(sstl);
  htable_vertex_release(&sstl->vertex2id);
  MEM_RM(sstl->allocator, sstl);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
sstl_create
  (struct logger* log,
   struct mem_allocator* mem_allocator,
   const int verbose,
   struct sstl** out_sstl)
{
  struct mem_allocator* allocator = NULL;
  struct logger* logger = NULL;
  struct sstl* sstl = NULL;
  res_T res = RES_OK;

  if(!out_sstl) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = mem_allocator ? mem_allocator : &mem_default_allocator;
  logger = log ? log : LOGGER_DEFAULT;

  sstl = MEM_CALLOC(allocator, 1, sizeof(struct sstl));
  if(!sstl) {
    if(verbose) {
      logger_print(logger, LOG_ERROR,
        "Couldn't allocate the Star-STL device.\n");
    }
    res = RES_MEM_ERR;
    goto error;
  }

  ref_init(&sstl->ref);
  sstl->allocator = allocator;
  sstl->logger = logger;
  sstl->verbose = verbose;
  htable_vertex_init(allocator, &sstl->vertex2id);

exit:
  if(out_sstl) *out_sstl = sstl;
  return res;
error:
  if(sstl) {
    SSTL(ref_put(sstl));
    sstl = NULL;
  }
  goto exit;
}

res_T
sstl_ref_get(struct sstl* sstl)
{
  if(!sstl) return RES_BAD_ARG;
  ref_get(&sstl->ref);
  return RES_OK;
}

res_T
sstl_ref_put(struct sstl* sstl)
{
  if(!sstl) return RES_BAD_ARG;
  ref_put(&sstl->ref, sstl_release);
  return RES_OK;

}

res_T
sstl_load_ascii(struct sstl* sstl, const char* filename)
{
  if(!sstl || !filename) return RES_BAD_ARG;
  return load_base(sstl, filename, TRY_READ_ASCII);
}

res_T
sstl_load_binary(struct sstl* sstl, const char* filename)
{
  if(!sstl || !filename) return RES_BAD_ARG;
  return load_base(sstl, filename, TRY_READ_BINARY);
}

res_T
sstl_load(struct sstl* sstl, const char* filename)
{
  if(!sstl || !filename) return RES_BAD_ARG;
  return load_base(sstl, filename, TRY_READ_ALL);
}

res_T
sstl_load_stream_ascii(struct sstl* sstl, FILE* stream)
{
  if(!sstl || !stream) return RES_BAD_ARG;
  return load_stream(sstl, stream, "STREAM", TRY_READ_ASCII);
}

res_T
sstl_load_stream_binary(struct sstl* sstl, FILE* stream)
{
  if(!sstl || !stream) return RES_BAD_ARG;
  return load_stream(sstl, stream, "STREAM", TRY_READ_BINARY);
}

res_T
sstl_load_stream(struct sstl* sstl, FILE* stream)
{
  if(!sstl || !stream) return RES_BAD_ARG;
  return load_stream(sstl, stream, "STREAM", TRY_READ_ALL);
}

res_T
sstl_get_desc(struct sstl* sstl, struct sstl_desc* desc)
{
  if(!sstl || !desc
      /* OK_ASCII xor OK_BIN */
      || (sstl->solid.read_type & OK_BINARY) == (sstl->solid.read_type & OK_ASCII))
    return RES_BAD_ARG;
  desc->solid_name = sstl->solid.name;
  desc->read_type = (sstl->solid.read_type & OK_ASCII) ? SSTL_ASCII : SSTL_BINARY;
  desc->vertices_count = sa_size(sstl->solid.vertices);
  ASSERT(desc->vertices_count % 3 == 0);
  desc->vertices_count /= 3/* # coords per vertex */;
  desc->triangles_count = sa_size(sstl->solid.indices);
  ASSERT(desc->triangles_count % 3 == 0);
  desc->triangles_count /= 3/* # indices per triange */;
  desc->vertices = sstl->solid.vertices;
  desc->indices = sstl->solid.indices;
  desc->normals = sstl->solid.normals;
  return RES_OK;
}

res_T
sstl_write
  (const struct sstl_write_data* data,
   const int binary,
   const char* filename)
{
  FILE* file;
  res_T res = RES_OK;

  if(!data || !filename)
    return RES_BAD_ARG;

  file = fopen(filename, (binary ? "wb" : "w"));
  if(!file) {
    if(data->print_error_log) {
      data->print_error_log(data, "Error opening `%s'.\n", filename);
    }
    return RES_IO_ERR;
  }
  res = write_stream(data, file, filename, binary);
  fclose(file);
  return res;
}

res_T
sstl_write_stream
  (const struct sstl_write_data* data,
   const int binary,
   FILE* stream)
{
  if(!data || !stream) return RES_BAD_ARG;
  return write_stream(data, stream, "STREAM", binary);
}

#ifdef COMPILER_CL
  #pragma warning(pop)
#endif

