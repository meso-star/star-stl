/* Copyright (C) 2015, 2016, 2019, 2021, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sstl.h"
#include "test_sstl_utils.h"

#include <rsys/clock_time.h>
#include <rsys/float3.h>
#include <rsys/logger.h>
#include <rsys/rsys.h>

static void
test_basic(struct sstl* sstl)
{
  static const char* test0 =
    "solid\n"
    "  facet normal 0.0 -1.0 0.0\n"
    "    outer loop\n"
    "      vertex 0.0 0.0 0.0\n"
    "      vertex 1.0 0.0 0.0\n"
    "      vertex 0.0 0.0 1.0\n"
    "    endloop\n"
    "  endfacet\n"
    "endsolid";
  static const char* test1 =
    "solid my_solid\n"
    "\n"
    "  facet normal 0.0 -1.0 0.0\n"
    "    outer loop     hophophophophop\n"
    "      vertex\t  0.0      0.0 0.0\n"
    "      vertex 1.0   0.0 0.0     \taaa\n"
    "      vertex 0.0   0.0     1.0\n"
    "    endloop   \n"
    "  endfacet   \t\t\t noise\n"
    "endsolid pouet\n";
  static const char* test2 =
    "solid my_solid\n"
    "endsolid my_solid\n";
  static const char* test3 =
    "solid\n"
    "  facet normal 0.0 0.0 0.0\n"
    "    outer loop\n"
    "      vertex 0.0 0.0 0.0\n"
    "      vertex 1.0 0.0 0.0\n"
    "      vertex 0.0 0.0 1.0\n"
    "    endloop\n"
    "  endfacet\n"
    "endsolid";
  static const char* bad[] = {
    "solid\n"
    "  facet normal 0.0 -1.0 0.0\n"
    "    outer loop\n"
    "      vertex 0.0 0.0 0.0\n"
    "      vertex 1.0 0.0 0.0\n"
    "      vertex 0.0 0.0 1.0\n"
    "    endloop\n"
    "  endfacet\n"
    ,
    "  facet normal 0.0 -1.0 0.0\n"
    "    outer loop\n"
    "      vertex 0.0 0.0 0.0\n"
    "      vertex 1.0 0.0 0.0\n"
    "      vertex 0.0 0.0 1.0\n"
    "    endloop\n"
    "  endfacet\n"
    "endsolid\n"
    ,
    "solid\n"
    "  facet 0.0 -1.0 0.0\n"
    "    outer loop\n"
    "      vertex 0.0 0.0 0.0\n"
    "      vertex 1.0 0.0 0.0\n"
    "      vertex 0.0 0.0 1.0\n"
    "    endloop\n"
    "  endfacet\n"
    "endsolid\n"
    ,
    "solid\n"
    "  normal 0.0 -1.0 0.0\n"
    "    outer loop\n"
    "      vertex 0.0 0.0 0.0\n"
    "      vertex 1.0 0.0 0.0\n"
    "      vertex 0.0 0.0 1.0\n"
    "    endloop\n"
    "  endfacet\n"
    "endsolid\n"
    ,
    "solid\n"
    "  facet normal 0.0 -1.0 0.0\n"
    "    outer loop\n"
    "      vertex 0.0 0.0 a.0\n"
    "      vertex 1.0 0.0 0.0\n"
    "      vertex 0.0 0.0 1.0\n"
    "    endloop\n"
    "  endfacet\n"
    "endsolid\n"
    ,
    "solid\n"
    "  facet normal 0.0 -1.0 0.0\n"
    "    outer loop\n"
    "      vertex 1.0 0.0 0.0\n"
    "      vertex 0.0 0.0 1.0\n"
    "    endloop\n"
    "  endfacet\n"
    "endsolid\n"
    ,
    "solid\n"
    "  facet normal 0.0 -1.0 0.0\n"
    "    vertex 0.0 0.0 0.0\n"
    "    vertex 1.0 0.0 0.0\n"
    "    vertex 0.0 0.0 1.0\n"
    "  endfacet\n"
    "endsolid\n"
  };
  const size_t nbads = sizeof(bad)/sizeof(const char*);
  float tmp[3];
  struct sstl_desc desc;
  FILE* file;
  size_t i;
  struct sstl_write_data wd;

  CHK(sstl != NULL);

  file = fopen("test_basic.stl", "w");
  CHK(file != NULL);
  fwrite(test0, sizeof(char), strlen(test0), file);
  fclose(file);

  CHK(sstl_load_ascii(NULL, NULL) == RES_BAD_ARG);
  CHK(sstl_load_ascii(sstl, NULL) == RES_BAD_ARG);
  CHK(sstl_load_ascii(NULL, "test_basic.stl") == RES_BAD_ARG);
  CHK(sstl_load_ascii(sstl, "none.stl") == RES_IO_ERR);
  CHK(sstl_load_ascii(sstl, "test_basic.stl") == RES_OK);

  CHK(sstl_load_binary(NULL, NULL) == RES_BAD_ARG);
  CHK(sstl_load_binary(sstl, NULL) == RES_BAD_ARG);
  CHK(sstl_load_binary(NULL, "test_basic.stl") == RES_BAD_ARG);
  CHK(sstl_load_binary(sstl, "none.stl") == RES_IO_ERR);
  CHK(sstl_load_binary(sstl, "test_basic.stl") == RES_BAD_ARG);

  CHK(sstl_load(NULL, NULL) == RES_BAD_ARG);
  CHK(sstl_load(sstl, NULL) == RES_BAD_ARG);
  CHK(sstl_load(NULL, "test_basic.stl") == RES_BAD_ARG);
  CHK(sstl_load(sstl, "none.stl") == RES_IO_ERR);
  CHK(sstl_load(sstl, "test_basic.stl") == RES_OK);

  CHK(sstl_get_desc(NULL, NULL) == RES_BAD_ARG);
  CHK(sstl_get_desc(sstl, NULL) == RES_BAD_ARG);
  CHK(sstl_get_desc(NULL, &desc) == RES_BAD_ARG);
  CHK(sstl_get_desc(sstl, &desc) == RES_OK);

  CHK(desc.solid_name == NULL);
  CHK(desc.vertices_count == 3);
  CHK(desc.triangles_count == 1);
  CHK(desc.indices[0] == 0);
  CHK(desc.indices[1] == 1);
  CHK(desc.indices[2] == 2);
  CHK(f3_eq(desc.vertices + 0*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + 1*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + 2*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.normals, f3(tmp, 0.f, -1.f, 0.f)) == 1);

  file = tmpfile();
  CHK(file != NULL);
  fwrite(test1, sizeof(char), strlen(test1), file);

  rewind(file);
  CHK(sstl_load_stream_ascii(NULL, NULL) == RES_BAD_ARG);
  CHK(sstl_load_stream_ascii(sstl, NULL) == RES_BAD_ARG);
  CHK(sstl_load_stream_ascii(NULL, file) == RES_BAD_ARG);
  CHK(sstl_load_stream_ascii(sstl, file) == RES_OK);

  rewind(file);
  CHK(sstl_load_stream_binary(NULL, NULL) == RES_BAD_ARG);
  CHK(sstl_load_stream_binary(sstl, NULL) == RES_BAD_ARG);
  CHK(sstl_load_stream_binary(NULL, file) == RES_BAD_ARG);
  CHK(sstl_load_stream_binary(sstl, file) == RES_BAD_ARG);

  rewind(file);
  CHK(sstl_load_stream(NULL, NULL) == RES_BAD_ARG);
  CHK(sstl_load_stream(sstl, NULL) == RES_BAD_ARG);
  CHK(sstl_load_stream(NULL, file) == RES_BAD_ARG);
  CHK(sstl_load_stream_ascii(sstl, file) == RES_OK);
  fclose(file);

  CHK(sstl_pack_write_data(NULL, &wd) == RES_BAD_ARG);
  CHK(sstl_pack_write_data(sstl, NULL) == RES_BAD_ARG);
  CHK(sstl_pack_write_data(NULL, NULL) == RES_BAD_ARG);
  CHK(sstl_pack_write_data(sstl, &wd) == RES_OK);
  CHK(sstl_write(&wd, 1, "test_basic.stl") == RES_OK);
  CHK(sstl_load_binary(sstl, "test_basic.stl") == RES_OK);

  CHK(sstl_pack_write_data(sstl, &wd) == RES_OK);
  wd.data = NULL;
  CHK(sstl_write(&wd, 1, "test_basic.stl") == RES_BAD_ARG);

  CHK(sstl_pack_write_data(sstl, &wd) == RES_OK);
  wd.get_triangle_normal = NULL;
  CHK(sstl_write(&wd, 1, "test_basic.stl") == RES_BAD_ARG);

  CHK(sstl_pack_write_data(sstl, &wd) == RES_OK);
  wd.get_triangle_vertices = NULL;
  CHK(sstl_write(&wd, 1, "test_basic.stl") == RES_BAD_ARG);

  CHK(sstl_pack_write_data(sstl, &wd) == RES_OK);
  wd.triangles_count += 1;
  CHK(sstl_write(&wd, 1, "test_basic.stl") == RES_BAD_ARG);

  file = tmpfile();
  CHK(file != NULL);
  CHK(sstl_pack_write_data(sstl, &wd) == RES_OK);
  CHK(sstl_write_stream(&wd, 1, file) == RES_OK);
  rewind(file);
  CHK(sstl_load_stream_binary(sstl,file) == RES_OK);
  fclose(file);

  CHK(sstl_get_desc(sstl, &desc) == RES_OK);
  CHK(desc.solid_name == NULL);
  CHK(desc.vertices_count == 3);
  CHK(desc.triangles_count == 1);
  CHK(desc.indices[0] == 0);
  CHK(desc.indices[1] == 1);
  CHK(desc.indices[2] == 2);
  CHK(f3_eq(desc.vertices + 0*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + 1*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + 2*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.normals, f3(tmp, 0.f, -1.f, 0.f)) == 1);

  file = tmpfile();
  fwrite(test2, sizeof(char), strlen(test2), file);
  rewind(file);
  CHK(sstl_load_stream(sstl, file) == RES_OK);
  fclose(file);

  CHK(sstl_get_desc(sstl, &desc) == RES_OK);
  CHK(strcmp(desc.solid_name, "my_solid") == 0);
  CHK(desc.vertices_count == 0);
  CHK(desc.triangles_count == 0);

  file = tmpfile();
  fwrite(test3, sizeof(char), strlen(test3), file);
  rewind(file);
  CHK(sstl_load_stream_binary(sstl, file) == RES_BAD_ARG);
  fclose(file);

  FOR_EACH(i, 0, nbads) {
    file = tmpfile();
    fwrite(bad[i], sizeof(char), strlen(bad[i]), file);
    rewind(file);
    CHK(sstl_load_stream(sstl, file) == RES_BAD_ARG);
    fclose(file);
  }

  CHK(sstl_pack_write_data(NULL, NULL) == RES_BAD_ARG);
  CHK(sstl_pack_write_data(sstl, NULL) == RES_BAD_ARG);
  CHK(sstl_pack_write_data(NULL, &wd) == RES_BAD_ARG);

  CHK(sstl_write(NULL, 0, NULL) == RES_BAD_ARG);
  CHK(sstl_write(&wd, 0, NULL) == RES_BAD_ARG);
  CHK(sstl_write(NULL, 0, "test_basic.stl") == RES_BAD_ARG);
  CHK(sstl_write(&wd, 0, "") == RES_IO_ERR);
  CHK(sstl_write(&wd, 1, "") == RES_IO_ERR);

  file = fopen("test_basic.stl", "w");
  CHK(file != NULL);
  fwrite(test0, sizeof(char), strlen(test0), file);
  fclose(file);

  CHK(sstl_load_ascii(sstl, "test_basic.stl") == RES_OK);
  CHK(sstl_pack_write_data(sstl, &wd) == RES_OK);
  CHK(sstl_write(&wd, 0, "test_basic.stl") == RES_OK);
  CHK(sstl_load(sstl, "test_basic.stl") == RES_OK);
  CHK(sstl_get_desc(sstl, &desc) == RES_OK);

  CHK(desc.solid_name == NULL);
  CHK(desc.vertices_count == 3);
  CHK(desc.triangles_count == 1);
  CHK(desc.indices[0] == 0);
  CHK(desc.indices[1] == 1);
  CHK(desc.indices[2] == 2);
  CHK(f3_eq(desc.vertices + 0*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + 1*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + 2*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.normals, f3(tmp, 0.f, -1.f, 0.f)) == 1);

  file = fopen("test_basic.stl", "w");
  CHK(file != NULL);
  fwrite(test0, sizeof(char), strlen(test0), file);
  fclose(file);

  CHK(sstl_load(sstl, "test_basic.stl") == RES_OK);
  CHK(sstl_pack_write_data(sstl, &wd) == RES_OK);
  CHK(sstl_write(&wd, 1, "test_basic2.stl") == RES_OK);
  CHK(sstl_load(sstl, "test_basic2.stl") == RES_OK);
  CHK(sstl_get_desc(sstl, &desc) == RES_OK);

  CHK(desc.solid_name == NULL);
  CHK(desc.vertices_count == 3);
  CHK(desc.triangles_count == 1);
  CHK(desc.indices[0] == 0);
  CHK(desc.indices[1] == 1);
  CHK(desc.indices[2] == 2);
  CHK(f3_eq(desc.vertices + 0*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + 1*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + 2*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.normals, f3(tmp, 0.f, -1.f, 0.f)) == 1);
}

static void
test_tetrahedron(struct sstl* sstl)
{
  static const char* tetrahedron[] = {
    "solid cube_corner\n",
    "  facet normal 0.0 -1.0 0.0\n",
    "    outer loop\n",
    "      vertex 0.0 0.0 0.0\n",
    "      vertex 1.0 0.0 0.0\n",
    "      vertex 0.0 0.0 1.0\n",
    "    endloop\n",
    "  endfacet\n",
    "  facet normal 0.0 0.0 -1.0\n",
    "    outer loop\n",
    "      vertex 0.0 0.0 0.0\n",
    "      vertex 0.0 1.0 0.0\n",
    "      vertex 1.0 0.0 0.0\n",
    "    endloop\n",
    "  endfacet\n",
    "  facet normal -1.0 0.0 0.0\n",
    "    outer loop\n",
    "      vertex 0.0 0.0 0.0\n",
    "      vertex 0.0 0.0 1.0\n",
    "      vertex 0.0 1.0 0.0\n",
    "    endloop\n",
    "  endfacet\n",
    "  facet normal 0.577 0.577 0.577\n",
    "    outer loop\n",
    "      vertex 1.0 0.0 0.0\n",
    "      vertex 0.0 1.0 0.0\n",
    "      vertex 0.0 0.0 1.0\n",
    "    endloop\n",
    "  endfacet\n",
    "endsolid\n"
  };
  FILE* file;
  const size_t nlines = sizeof(tetrahedron)/sizeof(const char*);
  struct sstl_desc desc;
  float tmp[3];
  size_t i;
  struct sstl_write_data wd;

  CHK(sstl != NULL);

  file = tmpfile();
  CHK(file != NULL);
  FOR_EACH(i, 0, nlines)
    fwrite(tetrahedron[i], sizeof(char), strlen(tetrahedron[i]), file);
  rewind(file);

  CHK(sstl_load_stream(sstl, file) == RES_OK);

  CHK(sstl_get_desc(sstl, &desc) == RES_OK);
  CHK(strcmp(desc.solid_name, "cube_corner") == 0);
  CHK(desc.vertices_count == 4);
  CHK(desc.triangles_count == 4);

  CHK(f3_eq(desc.vertices + desc.indices[0]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[1]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[2]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[3]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[4]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[5]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[6]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[7]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[8]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[9]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[10]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[11]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);

  CHK(f3_eq(desc.normals + 0*3, f3(tmp, 0.f,-1.f, 0.f)) == 1);
  CHK(f3_eq(desc.normals + 1*3, f3(tmp, 0.f, 0.f,-1.f)) == 1);
  CHK(f3_eq(desc.normals + 2*3, f3(tmp,-1.f, 0.f, 0.f)) == 1);
  f3_normalize(tmp, f3(tmp, 1.f, 1.f, 1.f));
  CHK(f3_eq_eps(desc.normals + 3*3, tmp, 1.e-6f) == 1);

  rewind(file);
  CHK(sstl_load_stream_binary(sstl, file) == RES_BAD_ARG);
  rewind(file);
  CHK(sstl_load_stream_ascii(sstl, file) == RES_OK);

  CHK(sstl_get_desc(sstl, &desc) == RES_OK);
  CHK(strcmp(desc.solid_name, "cube_corner") == 0);
  CHK(desc.vertices_count == 4);
  CHK(desc.triangles_count == 4);

  CHK(f3_eq(desc.vertices + desc.indices[0]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[1]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[2]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[3]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[4]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[5]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[6]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[7]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[8]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[9]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[10]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[11]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);

  CHK(f3_eq(desc.normals + 0*3, f3(tmp, 0.f,-1.f, 0.f)) == 1);
  CHK(f3_eq(desc.normals + 1*3, f3(tmp, 0.f, 0.f,-1.f)) == 1);
  CHK(f3_eq(desc.normals + 2*3, f3(tmp,-1.f, 0.f, 0.f)) == 1);
  f3_normalize(tmp, f3(tmp, 1.f, 1.f, 1.f));
  CHK(f3_eq_eps(desc.normals + 3*3, tmp, 1.e-6f) == 1);

  CHK(sstl_pack_write_data(sstl, &wd) == RES_OK);
  CHK(sstl_write(&wd, 0, "corner.stl") == RES_OK);
  CHK(sstl_write(&wd, 1, "corner_bin.stl") == RES_OK);

  CHK(sstl_load_ascii(sstl, "corner_bin.stl") == RES_BAD_ARG);
  CHK(sstl_load_binary(sstl, "corner.stl") == RES_BAD_ARG);

  CHK(sstl_load(sstl, "corner.stl") == RES_OK);

  CHK(sstl_get_desc(sstl, &desc) == RES_OK);
  CHK(strcmp(desc.solid_name, "cube_corner") == 0);
  CHK(desc.vertices_count == 4);
  CHK(desc.triangles_count == 4);

  CHK(f3_eq(desc.vertices + desc.indices[0]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[1]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[2]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[3]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[4]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[5]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[6]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[7]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[8]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[9]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[10]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[11]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);

  CHK(f3_eq(desc.normals + 0*3, f3(tmp, 0.f,-1.f, 0.f)) == 1);
  CHK(f3_eq(desc.normals + 1*3, f3(tmp, 0.f, 0.f,-1.f)) == 1);
  CHK(f3_eq(desc.normals + 2*3, f3(tmp,-1.f, 0.f, 0.f)) == 1);
  f3_normalize(tmp, f3(tmp, 1.f, 1.f, 1.f));
  CHK(f3_eq_eps(desc.normals + 3*3, tmp, 1.e-6f) == 1);

  CHK(sstl_load_ascii(sstl, "corner.stl") == RES_OK);

  CHK(sstl_get_desc(sstl, &desc) == RES_OK);
  CHK(strcmp(desc.solid_name, "cube_corner") == 0);
  CHK(desc.vertices_count == 4);
  CHK(desc.triangles_count == 4);

  CHK(f3_eq(desc.vertices + desc.indices[0]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[1]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[2]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[3]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[4]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[5]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[6]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[7]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[8]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[9]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[10]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[11]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);

  CHK(f3_eq(desc.normals + 0*3, f3(tmp, 0.f,-1.f, 0.f)) == 1);
  CHK(f3_eq(desc.normals + 1*3, f3(tmp, 0.f, 0.f,-1.f)) == 1);
  CHK(f3_eq(desc.normals + 2*3, f3(tmp,-1.f, 0.f, 0.f)) == 1);
  f3_normalize(tmp, f3(tmp, 1.f, 1.f, 1.f));
  CHK(f3_eq_eps(desc.normals + 3*3, tmp, 1.e-6f) == 1);

  CHK(sstl_load(sstl, "corner_bin.stl") == RES_OK);

  CHK(sstl_get_desc(sstl, &desc) == RES_OK);
  CHK(desc.solid_name == NULL); /* binary files don't store a name */
  CHK(desc.vertices_count == 4);
  CHK(desc.triangles_count == 4);

  CHK(f3_eq(desc.vertices + desc.indices[0]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[1]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[2]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[3]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[4]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[5]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[6]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[7]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[8]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[9]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[10]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[11]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);

  CHK(f3_eq(desc.normals + 0*3, f3(tmp, 0.f,-1.f, 0.f)) == 1);
  CHK(f3_eq(desc.normals + 1*3, f3(tmp, 0.f, 0.f,-1.f)) == 1);
  CHK(f3_eq(desc.normals + 2*3, f3(tmp,-1.f, 0.f, 0.f)) == 1);
  f3_normalize(tmp, f3(tmp, 1.f, 1.f, 1.f));
  CHK(f3_eq_eps(desc.normals + 3*3, tmp, 1.e-6f) == 1);

  CHK(sstl_load_binary(sstl, "corner_bin.stl") == RES_OK);

  CHK(sstl_get_desc(sstl, &desc) == RES_OK);
  CHK(desc.solid_name == NULL); /* binary files don't store a name */
  CHK(desc.vertices_count == 4);
  CHK(desc.triangles_count == 4);

  CHK(f3_eq(desc.vertices + desc.indices[0]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[1]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[2]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[3]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[4]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[5]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[6]*3, f3(tmp, 0.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[7]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[8]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[9]*3, f3(tmp, 1.f, 0.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[10]*3, f3(tmp, 0.f, 1.f, 0.f)) == 1);
  CHK(f3_eq(desc.vertices + desc.indices[11]*3, f3(tmp, 0.f, 0.f, 1.f)) == 1);

  CHK(f3_eq(desc.normals + 0*3, f3(tmp, 0.f,-1.f, 0.f)) == 1);
  CHK(f3_eq(desc.normals + 1*3, f3(tmp, 0.f, 0.f,-1.f)) == 1);
  CHK(f3_eq(desc.normals + 2*3, f3(tmp,-1.f, 0.f, 0.f)) == 1);
  f3_normalize(tmp, f3(tmp, 1.f, 1.f, 1.f));
  CHK(f3_eq_eps(desc.normals + 3*3, tmp, 1.e-6f) == 1);
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct sstl* sstl;
  int i;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  CHK(sstl_create(NULL, &allocator, 1, &sstl) == RES_OK);

  test_basic(sstl);
  test_tetrahedron(sstl);

  FOR_EACH(i, 1, argc) {
    struct sstl_desc desc;
    char buf[512];
    struct time t0, t1;

    printf("loading %s", argv[i]);
    fflush(stdout);

    time_current(&t0);
    CHK(sstl_load(sstl, argv[i]) == RES_OK);
    time_current(&t1);
    time_sub(&t0, &t1, &t0);
    time_dump(&t0, TIME_MIN|TIME_SEC|TIME_MSEC, NULL, buf, sizeof(buf));

    CHK(sstl_get_desc(sstl, &desc) == RES_OK);

    printf(" - #vertices = %lu; #triangles = %lu - %s\n",
      (unsigned long)desc.vertices_count,
      (unsigned long)desc.triangles_count,
      buf);
  }

  CHK(sstl_ref_put(sstl) == RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

