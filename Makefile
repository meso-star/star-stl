# Copyright (C) 2015, 2016, 2019, 2021, 2023 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libsstl.a
LIBNAME_SHARED = libsstl.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC = src/sstl.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then\
	     echo "$(LIBNAME)";\
	   else\
	     echo "$(LIBNAME_SHARED)";\
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) -std=c99 $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(RSYS_LIBS) -lm

$(LIBNAME_STATIC): libsstl.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libsstl.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found"; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) -std=c99 $(CFLAGS_SO) $(RSYS_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) -std=c99 $(CFLAGS_SO) $(RSYS_CFLAGS) -DSSTL_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g' \
	    -e 's#@VERSION@#$(VERSION)#g' \
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g' \
	    sstl.pc.in > sstl.pc

sstl-local.pc: sstl.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g' \
	    sstl.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" sstl.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/sstl.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/star-stl" COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/sstl.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-stl/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-stl/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/sstl.h"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test libsstl.o sstl.pc sstl-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_sstl.c\
 src/test_sstl_load.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
SSTL_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags sstl-local.pc)
SSTL_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs sstl-local.pc)

build_tests: build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test $$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

test: build_tests
	@$(SHELL) make.sh run_test $(TEST_SRC)

.test: Makefile
	@$(SHELL) make.sh config_test $(TEST_SRC) > $@

clean_test:
	rm -f corner.stl corner_bin.stl test_basic.stl test_basic2.stl
	$(SHELL) make.sh clean_test $(TEST_SRC)

$(TEST_DEP): config.mk sstl-local.pc
	@$(CC) -std=c89 $(CFLAGS_EXE) $(RSYS_CFLAGS) $(SSTL_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk sstl-local.pc
	$(CC) -std=c89 $(CFLAGS_EXE) $(RSYS_CFLAGS) $(SSTL_CFLAGS) -c $(@:.o=.c) -o $@

test_sstl test_sstl_load: config.mk sstl-local.pc $(LIBNAME)
	$(CC) -std=c89 $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(SSTL_LIBS) $(RSYS_LIBS) -lm
